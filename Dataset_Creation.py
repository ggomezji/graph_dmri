#%%
import numpy as np
import os
from scipy import ndimage
import nibabel as nib
from nilearn import maskers, image
from nilearn.maskers import NiftiLabelsMasker
from dipy.io import read_bvals_bvecs
from dipy.core.gradients import gradient_table
import torch
from torch import nn
from torch.utils import data
from torchvision import datasets, transforms # If you want to apply transformations to the images
from PIL import Image  # If you want to work with image files 
#%%
torch.__version__
# Setup device-agnostic code
device = "cuda" if torch.cuda.is_available() else "cpu"

#%%
class CustomDataset(data.Dataset):
    def __init__(self, root_dir, labels_desired, transform=None):
        'characterizes a dataset for pytorch'
        self.root_dir = root_dir  # Directory containing data
        self.labels_desired = labels_desired
        self.transform = transform  # Optional transformations to apply to the images
        self.image_files = self.file_directories() # List of image file names

    def file_directories(self):
        """
        Get the directories of the different files needed
        """
        image_files = []
        for subject_dir in os.listdir(self.root_dir):
            SUBJECT_PATH = os.path.join(self.root_dir, subject_dir)
            RAW_DATA = os.path.join(SUBJECT_PATH, "T1w/Diffusion/data.nii.gz")
            # Filter the data that does not contain diffusion data
            # Check if the 'T1w' data exists in the subject directory
            #t1w_dir = os.path.join(SUBJECT_PATH, "T1w")
            if not subject_dir.startswith('1'):
                continue
            if not os.path.exists(RAW_DATA):
                # 'T1w' directory does not exist, skip this subject
                continue
            # Paths to data
            MASK_PATH = os.path.join(SUBJECT_PATH,  "T1w/aparc.a2009s+aseg.nii.gz")

        # Not considered at the moment. I just want to create a dataset of the images
        #VENT_PATH = SUBJECT_PATH + "/T1w/aparc.a2009s+aseg.nii.gz"

        # Measurements
            B_VALS_PATH = os.path.join(SUBJECT_PATH, "T1w/Diffusion/bvals")
            B_VECS_PATH = os.path.join(SUBJECT_PATH, "T1w/Diffusion/bvecs")
            image_files.append((SUBJECT_PATH, RAW_DATA, B_VALS_PATH, B_VECS_PATH))
        return image_files #, VENT_PATH, B_VALS, B_VECS

    def __len__(self):
        'denotes the total number of samples'
        return len(self.image_files)  # Number of images in the dataset

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        # Load an image
        subject_path, raw_data_path, bvals_path, bvecs_path = self.image_files[idx]
        
        # Load RAW_DATA and MASK_PATH using nibabel (you may need to adjust this part)
        raw_image = nib.load(raw_data_path)
        raw_data = raw_image.get_fdata()
        #mask_image = nib.load(mask_path)
        #mask_data = mask_image.get_fdata()

        # Load b-values and b-vecs
        bvals, bvecs = read_bvals_bvecs(bvals_path, bvecs_path)

        # Create a PyTorch Geometric Data object to represent the graph
        node_features = []  # List to store node features
        edge_index = []  # List to store edges

        for x in range(raw_data.shape[0]):
            for y in range(raw_data.shape[1]):
                for z in range(raw_data.shape[2]):
                    signal_intensity = raw_data[x, y, z]
        '''
        # Apply transformations if specified
        if self.transform:
            data_image = self.transform(mask_image, mask_data, self.labels_desired, raw_image)

        # Return the image and its corresponding label (if applicable)
        return data_image'''
        return raw_data
#%%
# Definition of parameters
# Define Paths of stored data and graph transformations
DATA_DIRECTORY = '/home/mind/ggomezji/data/HCP900'
LABELS_DESIRED = [1, 2, 3, 4, 5, 6]

# Create an instance of your custom dataset
custom_dataset = CustomDataset(DATA_DIRECTORY, LABELS_DESIRED)
# Save the custom dataset without separating train and test
torch.save(custom_dataset, '/home/mind/ggomezji/data/HCP_dataset.pt')
files = custom_dataset.file_directories()

'''
#%%
# Definition of one subject for testing
subject_dir = '101915'
DATA_DIRECTORY = '/home/mind/ggomezji/data/HCP900'
SUBJECT_PATH = os.path.join(DATA_DIRECTORY, subject_dir)
B_VALS_PATH = os.path.join(SUBJECT_PATH, "T1w/Diffusion/bvals")
B_VECS_PATH = os.path.join(SUBJECT_PATH, "T1w/Diffusion/bvecs")
bvals, bvecs = read_bvals_bvecs(B_VALS_PATH, B_VECS_PATH)
gtab = gradient_table(bvals, bvecs)'''
#%%
batch_size = 32
dataloader = data.DataLoader(custom_dataset, batch_size=batch_size, shuffle=True)

''''
#%%
# Define the class of a custom transform
class CustomTransform:
    def __init__(self, mask_image, mask_data, labels_desired, raw_image, erosion=False):
        self.mask_image = mask_image
        self.mask_data = mask_data
        self.labels_desired = labels_desired
        self.raw_image = raw_image
        self.erosion = erosion
        self.data_masked = self.compute_mask()
    
    def compute_mask(self):
        """
        Apply a mask to the input image. It exists the possibility of applying erosion to the mask.

        Args:
            mask_image: Input 3D image
            mask_data: Input data of the mask
            labels_desired: Mask labels you want to extract
            subject_image: 3D image of the subject to study
            erosion (bool): Determine if you perform a 1x1x1 voxel erosion to the mask before applying

        Returns:
            data_masked: Data of the subject masked
        """
        if self.erosion == True: 

            unique_labels = np.unique(self.mask_data)
            unique_labels = unique_labels[unique_labels != 0].astype(np.int16)

            # Define the structuring element for erosion (1x1x1 cube)
            structuring_element = np.ones((1, 1, 1))

            # Create a new array to store the eroded mask
            eroded_mask_data = np.zeros_like(self.mask_data)

            # Iterate over each label and perform erosion
            for label in self.unique_labels:
                if label == 0:
                    # Skip erosion for background label (label 0)
                    continue
                
                # Create a binary mask for the current label
                label_mask = np.where(self.mask_data == label, 1, 0)
                
                # Perform erosion on the label mask using the structuring element
                eroded_label_mask = ndimage.binary_erosion(label_mask, structure=structuring_element)
                
                # Update the eroded mask array with the eroded label
                eroded_mask_data += eroded_label_mask * label

            # Create a new NIFTI image for the eroded mask
            eroded_mask_img = nib.Nifti1Image(eroded_mask_data, self.mask_image.affine, self.mask_image.header)

            # Save the eroded mask to a new NIFTI file
            #nib.save(eroded_mask_img, eroded_mask_path)

            self.mask_image = eroded_mask_img

        masker = NiftiLabelsMasker(labels_img=self.mask_image, labels=self.labels_desired)
        data_masked = masker.fit_transform(self.raw_image)   # To get the image we need the affine
        return data_masked

    def __call__(self):
        # Use the Mask function from your module
        transformed_data = self.compute_mask()

        return transformed_data

#%%
# Define transformation:
transform = transforms.Compose([
    CustomTransform,  # Apply the CustomTransform function
    transforms.ToTensor(),  # Convert to a PyTorch tensor
    # Add other transformations
])
#%%
# Define Paths of stored data and graph transformations
DATA_DIRECTORY = '/home/mind/ggomezji/data/HCP900'
LABELS_DESIRED = [1, 2, 3, 4, 5, 6]

# Create an instance of your custom dataset
custom_dataset = CustomDataset(DATA_DIRECTORY, LABELS_DESIRED, transform=transform)

# Save the custom dataset without separating train and test
torch.save(custom_dataset, './HCP_dataset.pt')

# Create a DataLoader to iterate through the dataset in batches
batch_size = 32
dataloader = data.DataLoader(custom_dataset, batch_size=batch_size, shuffle=True)
''''
