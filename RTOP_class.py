#IMPORT LIBRARIES
import numpy as np  
import nibabel as nib
from scipy import ndimage

# import nilearn
from nilearn.maskers import NiftiLabelsMasker

# import dipy
from dipy.io import read_bvals_bvecs
from dipy.core.gradients import gradient_table
from dipy.reconst import mapmri

class RTOP_computation():
    def __init__(self, bvalues, bvectors, imgSubject, imgMask, dataMask, imgVent=None, big_delta=0.0431, small_delta=0.0106):
        self.bvalues = bvalues
        self.bvectors = bvectors
        self.imgSubject = imgSubject
        self.imgMask = imgMask
        self.dataMask = dataMask
        self.imgVent =  imgVent
        self.big_delta = big_delta # seconds
        self.small_delta = small_delta  # seconds
        self.gtab = gradient_table(bvals=self.bvals, bvecs=self.bvecs,
                        big_delta=self.big_delta,
                        small_delta=self.small_delta)
    
    def RTOP_Vent(self):
        if self.imgVent is None:
            raise ValueError("imgVent is not provided.")
        # Define the model
        model = mapmri.MapmriModel(gtab=self.gtab, radial_order=6
                                            laplacian_regularization=True,
                                            laplacian_weighting=.2)
        
        masker = NiftiLabelsMasker(labels_img=self.imgVent, labels=[4, 5, 14, 15, 43, 44])
        data_masked_vent = masker.fit_transform(self.imgSubject)
        model_fit_vent = model.fit(data_masked_vent.T)

        # Computation RTOP
        rtop_vent = model_fit_vent.rtop()
        norm_rtop_vent = sum(rtop_vent)/len(rtop_vent)
        self.norm_rtop_vent = norm_rtop_vent
        return norm_rtop_vent


    def RTOP_Subject(self, voxels_to_erode=1):
        self.voxels_to_erode = voxels_to_erode

        # Compute the labels
        label_keys = np.unique(self.dataMask)
        label_keys = label_keys[label_keys != 0]
        labels = label_keys.astype(np.int16)

        # Erosion
        # Define the structuring element for erosion (3x3x3 cube)
        structuring_element = np.ones((self.voxels_to_erode, self.voxels_to_erode, self.voxels_to_erode))

        # Create a new array to store the eroded mask
        eroded_mask_data = np.zeros_like(self.dataMask)

        # Iterate over each label and perform erosion
        for label in labels:
            if label == 0:
                # Skip erosion for background label (label 0)
                continue
            
            # Create a binary mask for the current label
            label_mask = np.where(self.dataMask == label, 1, 0)
            
            # Perform erosion on the label mask using the structuring element
            eroded_label_mask = ndimage.binary_erosion(label_mask, structure=structuring_element)
            
            # Update the eroded mask array with the eroded label
            eroded_mask_data += eroded_label_mask * label

        # Create a new NIFTI image for the eroded mask
        imgMask_erosion = nib.Nifti1Image(eroded_mask_data, self.imgMask.affine, self.imgMask.header)

        # Compute the masks
        masker = NiftiLabelsMasker(labels_img=imgMask_erosion, labels=labels)
        data_masked_sub = masker.fit_transform(self.imgSubject)

        # Define the model
        model = mapmri.MapmriModel(gtab=self.gtab, radial_order=6
                                            laplacian_regularization=True,
                                            laplacian_weighting=.2)

        # Fit the model on the data
        model_fit_subject = model.fit(data_masked_sub.T)
        

        # Compute RTOP
        rtop_subject = model_fit_subject.rtop()
        
        self.rtop_subject = rtop_subject
        return rtop_subject
    
    def RTOP_normalized(self):
        if not hasattr(self, 'rtop_subject'):
            self.RTOP_Subject()  # Compute rtop_subject if it doesn't exist
        
        if not hasattr(self, 'norm_rtop_vent'):
            self.RTOP_Vent()  # Compute norm_rtop_vent if it doesn't exist
        rtop_subject_data_normalized = self.rtop_subject / self.norm_rtop_vent
        
        return rtop_subject_data_normalized

class RTOP_voxel():
    """
    Inputs:
        voxel: Information of the voxel we want to encode
        bvecs: directions of the images we are computing
        bval: b value we are working on
        sphere: representation of the information (vertices and edges)
    
    Return:
        rtop_voxel: RTOP value of the voxel in each of the bvecs direction
    """
    def __init__(self, voxel, bvecs, bval, sphere):
        self.voxel = voxel
        self.bvecs = bvecs
        self.bval = bval
        self.sphere = sphere
        self.unique_bvals = np.unique(np.round(self.bval / 1000) * 1000)
        self.unique_bvals = self.unique_bvals[self.unique_bvals > 0]
        self.gtab = gradient_table(bvals=bval, bvecs=bvecs)
        self.model = mapmri.MapmriModel(gtab=self.gtab)

    def values_extraction(self):
        model_fit_voxel = self.model.fit(self.voxel)
        
        gtab_0 = gradient_table(bvals=np.zeros((1,)), bvecs=np.array([[1, 0, 0]]))
        signal_0 = model_fit_voxel.predict(gtab_0)

        new_attenuation = {}
        for bval in self.unique_bvals:
            bvals = np.repeat(bval, len(self.sphere.vertices))
            sphere_gtab = gradient_table(bvals=bvals, bvecs=self.sphere.vertices)
            new_attenuation[bval] = model_fit_voxel.predict(sphere_gtab) / signal_0
        return new_attenuation