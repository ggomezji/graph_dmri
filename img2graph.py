# %%
# Import needed libraries
import numpy as np
import os
from scipy import ndimage
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import nibabel as nib
from dipy.core.gradients import gradient_table
from dipy.core.sphere import disperse_charges, Sphere, HemiSphere
from dipy.viz import window, actor
from nilearn import maskers, image
from nilearn.maskers import NiftiLabelsMasker
from dipy.io import read_bvals_bvecs
from dipy.core.gradients import gradient_table
import torch
from torch import nn
from torch.utils import data
from torchvision import datasets, transforms # If you want to apply transformations to the images
from mpl_toolkits.mplot3d import Axes3D
from nilearn import plotting
from pathlib import Path
from dipy.reconst.mapmri import MapmriModel
from os.path import expanduser


# %%
# Create the icosahedron and therefore the equidistant sphere points

from dipy.core.subdivide_octahedron import create_unit_sphere
sphere = create_unit_sphere(3)
plotting.plot_surf_stat_map((sphere.vertices, sphere.faces), sphere.vertices[:, 1])

# %%
folder = Path(expanduser('~/data'))
subject = '100307'
diffusion_folder = folder / subject / 'T1w' / 'Diffusion'
dwi = nib.load(diffusion_folder / 'data.nii.gz')
bvecs = np.loadtxt(diffusion_folder / 'bvecs')
bvals = np.loadtxt(diffusion_folder / 'bvals')

gtab = gradient_table(bvals=bvals, bvecs=bvecs)
model = MapmriModel(gtab=gtab)
voxel  = model.fit(dwi.get_fdata()[100, 50, 100])

# %%
gtab0 = gradient_table(bvals=np.repeat(1, 1), bvecs=np.array([[1, 0, 0]]))
gtab_sphere = gradient_table(bvals=np.repeat(3000, len(sphere.vertices)), bvecs=sphere.vertices)
b0_val = voxel.predict(gtab0)
attenuation = voxel.predict(gtab_sphere) / b0_val
plotting.plot_surf_stat_map((sphere.vertices * attenuation[:, None], sphere.faces), attenuation, vmax=1, vmin=.1);


# %%

# %%
# Definition of directories (Now, test for one subject)
subject_dir = '101915'
DATA_DIRECTORY = '/home/mind/ggomezji/data/HCP900'
SUBJECT_PATH = os.path.join(DATA_DIRECTORY, subject_dir)
RAW_DATA = os.path.join(SUBJECT_PATH, "T1w/Diffusion/data.nii.gz")
B_VALS_PATH = os.path.join(SUBJECT_PATH, "T1w/Diffusion/bvals")
B_VECS_PATH = os.path.join(SUBJECT_PATH, "T1w/Diffusion/bvecs")
bvals, bvecs = read_bvals_bvecs(B_VALS_PATH, B_VECS_PATH)

# Approximate bvalues to main directions
# Define the main b values and the maximum difference
main_values = [5, 1000, 2000, 3000]
max_difference = 15

# Your original vector
original_vector = bvals

# Create a function to replace values with the nearest main value
def replace_with_nearest(value):
    nearest_value = min(main_values, key=lambda x: abs(x - value))
    return nearest_value

# Replace values in the original vector
cleaned_bvals = np.array([replace_with_nearest(value) for value in original_vector])

# Compute gradient tables
# Original gradient table
gtab_original = gradient_table(bvals, bvecs)

#Modified gradient table
gtab = gradient_table(cleaned_bvals, bvecs)

"""
Create spheres for each of the bvalues and save them in a dictionary
    cleaned_bvals: bvals array where the values are approximated to the main value (5, 1000, 2000, 3000)
"""
# Initialize the spheres dictionary to store each of the spheres
spheres = {}

for bvalue in np.unique(cleaned_bvals):
    # Get all the indices for the value of b
    index = np.where(cleaned_bvals==bvalue)[0]
    # Generate the sphere with the vectors of b for the current b value
    sph = Sphere(x=bvecs[index, 0], y=bvecs[index, 1], z=bvecs[index, 2])
    # Store the sphere in a dictionary associated to the current b value
    spheres[bvalue] = sph


'''
    Sphere plotting
'''
# Plot of the spheres separatedly
for bvalue in np.unique(cleaned_bvals):
    # Get all the indices for the value of b
    index = np.where(cleaned_bvals==bvalue)[0]
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(bvalue*bvecs[index, 0], bvalue*bvecs[index, 1], bvalue*bvecs[index, 2])

    plt.show()


# Plot of the spheres together
# Create a list of spheres
spheres_representation = []
values = np.unique(cleaned_bvals)

for bvalue in np.unique(cleaned_bvals):
    index = np.where(cleaned_bvals == bvalue)[0]
    sph = Sphere(x=bvalue * bvecs[index, 0], y=bvalue * bvecs[index, 1], z=bvalue * bvecs[index, 2])
    spheres_representation.append(sph)

# Create a 3D plot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Define a list of colors for each sphere
colors = ['r', 'g', 'b', 'y']  # You can extend this list for more spheres

# Plot each sphere with a different color
for i, sphere in enumerate(spheres_representation):
    vertices = sphere.vertices
    x, y, z = values[i] * vertices[:, 0], values[i] * vertices[:, 1], values[i] * vertices[:, 2]
    ax.scatter(x, y, z, c=colors[i], label=f'Sphere {i + 1}', s=5)

# Set labels and legend
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
ax.legend()

# Show the plot
plt.show()

'''
Generate the graphs from the spheres
'''
# Initialize the spheres dictionary to store each of the spheres
graph_spheres = {}

# Function to create a graph
def create_connected_graph(sphere):
    vertices = sphere.vertices
    num_vertices = len(vertices)
    
    # Create an edge list for connecting vertices
    edge_list = []
    
    # Connect vertices that are neighbors on the sphere's surface
    for i in range(num_vertices):
        for j in range(i + 1, num_vertices):
            # Determine if vertices are neighbors (adjust the threshold as needed)
            if np.linalg.norm(vertices[i] - vertices[j]) < 0.1:
                edge_list.append((i, j))
    
    # Convert the edge list to PyTorch tensors
    edge_index = torch.tensor(list(zip(*edge_list)), dtype=torch.long)
    
    # Create a PyG Data object representing the graph
    graph = data.Data(edge_index=edge_index)
    
    return graph

# Iterate through each sphere
for bvalue, sphere in spheres.items():
    # Create a connected graph for the current sphere
    graph = create_connected_graph(sphere)
    
    # Store the graph associated with the bvalue
    graph_spheres[bvalue] = graph

"""
Get the information that will be stored in the vertices of the sphere
"""
from RTOP_class import RTOP_voxel
model = RTOP_voxel()
voxel_rtop = model.values_extraction()