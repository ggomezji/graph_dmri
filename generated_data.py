import numpy as np
import matplotlib.pyplot as plt
from dipy.core.gradients import gradient_table
from dipy.data import get_fnames
from dipy.io.image import save_nifti, load_nifti
from dipy.reconst.dti import TensorModel
from dipy.sims.voxel import single_tensor
import nibabel as nib

# Define simulation parameters
bval = 2000  # B-value (s/mm²)
num_bvecs = 90  # Number of random b-vectors
output_dir = '/home/mind/ggomezji/projects/graph_dmri/'  # Replace with your output directory

# Generate random b-vectors normalized
bvecs = np.random.rand(num_bvecs, 3)
bvecs /= np.linalg.norm(bvecs, axis=1)[:, np.newaxis]

# Create a gradient table
bvals = np.full(num_bvecs, bval)
gtab = gradient_table(bvals, bvecs)

# Simulate a synthetic diffusion tensor
evals = np.array([2.0e-3, 1.0e-4, 1.0e-4])  # Eigenvalues in mm²/s
tensor = single_tensor(gtab, S0=1, evals=evals)

# Affine computation
mean_direction = np.mean(bvecs, axis=0)
mean_direction /= np.linalg.norm(mean_direction) 

# Create a 4x4 identity matrix
affine = np.eye(4)

# Set the orientation of the gradient directions
affine[:3, :3] = mean_direction

# Save the diffusion tensor as a NIFTI file
save_nifti(output_dir + 'synthetic_tensor.nii.gz', tensor, affine)

# Create synthetic diffusion-weighted images
data, affine = load_nifti(output_dir + 'synthetic_tensor.nii.gz')
data = data.astype(np.float64)

# Apply the diffusion-weighted signal equation
signal = np.exp(-bval * np.dot(bvecs, np.dot(data, bvecs.T)))

# Add Rician noise to the signal
noise_std = 0.05
signal_with_noise = np.random.normal(signal, noise_std)
signal_with_noise = np.abs(signal_with_noise + 1j * np.random.normal(0, noise_std))

# Save the synthetic DWI as a NIFTI file
save_nifti(output_dir + 'synthetic_dwi.nii.gz', signal_with_noise, affine)

# Plot the synthetic DWI
plt.imshow(signal_with_noise.T, origin='lower', cmap='gray', aspect='auto')
plt.title('Synthetic DWI')
plt.show()
