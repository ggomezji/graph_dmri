# #%%writefile model_builder.py
#%%
import torch
import torch.nn as nn
from torch import Tensor
from torch_geometric.nn import GCNConv
import torch.nn.functional as F
from torch_geometric.nn import DMoNPooling #TopKPooling

#%%
class FromImageToGraph(nn.Module):
    def __init__(self)
#%%
class MicrostructureEstimationNet(nn.Module):
    def __init__(self, input_dim: int, hidden_units: int, output_dim: int, k: int) -> None:
        super(MicrostructureEstimationNet, self).__init__()
        # k = number of clusters
        self.graph_conv = GCNConv(input_dim, hidden_units)
        self.relu = nn.LeakyReLU(0.01) # Negative slope
        self.coarsen1 = DMoNPooling([hidden_units, hidden_units], k)
        self.coarsen2 = DMoNPooling([hidden_units, hidden_units], k/2)
        self.res_block1 = self.make_residual_block(hidden_units, hidden_units)
        self.res_block2 = self.make_residual_block(hidden_units, hidden_units)
        self.fc1 = nn.Linear(hidden_units, hidden_units)
        self.fc2 = nn.Linear(hidden_units, output_dim)

    def make_residual_block(self, input, hidden_units):
        return nn.Sequential(
            GCNConv(input, hidden_units),
            self.relu,
            GCNConv(hidden_units, hidden_units)
        )

    def forward(self, x: Tensor, edge_index: Tensor, hidden_units) -> Tensor:
        # x: Node feature matrix of shape [num_nodes, in_channels]
        # edge_index: Graph connectivity matrix of shape [num_edges, 2]
        
        # Graph Convolution Layer 1
        x = self.relu(self.graph_conv(x, hidden_units))

        # Residual Block 1
        x1 = self.res_block1(x, hidden_units)
        x = x + x1  # Residual skip

        # Coarsening 1
        x = self.coarsen1(x)

        # Residual Block 2
        x2 = self.res_block2(x, hidden_units)
        x = x + x2  # Residual skip

        # Coarsening 2
        x = self.coarsen2(x)

        # Graph Convolution Layer 2
        x = self.relu(self.graph_conv(x, hidden_units))

        # Fully-Connected Layers
        x = self.fc1(x)
        x = self.fc2(x)

        return x
    
#%%
# Example:
input_dim = 64  # Adjust the input dimension to match your data
hidden_dim = 128
output_dim = 64  # Adjust the output dimension as needed
num_blocks = 2  # Number of residual blocks

# Create the graph neural network model
model = MicrostructureEstimationNet(input_dim, hidden_dim, output_dim, num_blocks)
